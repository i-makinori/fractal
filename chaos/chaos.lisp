(ql:quickload :vgplot)

(defpackage :chaos1
  (:use :cl :vgplot)
  (:export ))

(in-package :chaos1)

;;;; parameters

(defparameter *val-start* 3.000)
(defparameter *val-end* 3.999)
(defparameter *val-step* 1.0e-3)

;;;; plotting

(defun vgplot-test ()
  (let ((x) (y))
    ;;(progn
    ;;(defvar x) (defvar y)
    (setf x (range 0 (* 2 pi) 0.01)) (setf y (map 'vector #'sin x))
    (plot x y "y = sin(x)")
    ;;(vgplot:plot x y "y = sin(x)")
    ;;(vgplot:plot #(1 2 3) #(0 -2 -17) ";silly example;")
    (vgplot:plot #(1 2 3) #(0 -2 -17) ";silly example;"
                 x y "y = sin(x)")
    (vgplot:title "Simple curve")
    (text 1.2 -14 "Plot vectors with legend and add a title")));;)


;;;; chaos


;; define of logistic map
;; x_{n+1} = a * x_{n} (1-x_{n})
;; a means parameter, Const.


#|
(defun logistic-map (val-r val-from val-to delta-r)
  (let ((val-range (range 
  (loop for val from val-from to
  (progn
    (let ((x) (y))
      ))))))))
|#

;; (defparameter *a* 2.00)

(defun logistic-step (a x_{t-1})
  (* a x_{t-1} (- 1.00 x_{t-1})))

(defun logistical-series (value_a x_{t0} num-iter)
  ;; make series of logistic function.
  ;; x_{t+1} = a * x_{t} (1-x_{t})
  ;; a is Constant Parameter.
  (let* ((y-vals (make-array (+ num-iter 0))))
    (setf (aref y-vals 0) x_{t0})
    (loop for i from 1 to (1- num-iter)
          do (setf (aref y-vals i)
                   (logistic-step value_a (aref y-vals (- i 1)))))
    y-vals))

;;(start 0.00)
;;(end 2.00)
;;(num-iter 100)
;;(x_0 0.01)
;;
;;(step (/ (- end start) num-iter))
;;(x-vals (range start end step))

(defun plot-series ()
  (let* ((num-iter 100)
         (step (/ (- 2.00 1.00) num-iter))
         (xs_* (range 1.00 2.00 step))
         (ys_1 (logistical-series 3.50100 0.1 num-iter)))
    (progn 
      (vgplot:plot xs_* ys_1  "logistic1")
      (vgplot:title "Simple curve")
      ;; (vgplot:text 1.2 -14 "some text"))
      ;;(values (length xs_*) (length ys_1))
    )))


(defun plot-cascade-orbit (var-start var-end var-step)
  (let* ((take-from 100) ;; 100
         (num-iter-at-each-a (+ 1024 take-from 2))
         (x_{t0} 0.1)
         ;;
         (xs_* (range var-start var-end var-step))
         (ys_n**
           (map 'list
                #'(lambda (a)
                    (logistical-series a x_{t0} num-iter-at-each-a))
                xs_*))
         ;;
         (whole-xs_*
           (apply #'concatenate 'vector
                  ;;(list '(1 2 3) '(4 5 6) #(3 4 5)))
                  (map 'list
                       (lambda (val-a)
                         (make-array (- num-iter-at-each-a take-from)
                                     :initial-element val-a))
                       xs_*)))

         (whole-ys_n**
           (apply #'concatenate 'vector
                  (map 'list
                       (lambda (ys_n*) (subseq ys_n* take-from))
                       ys_n**))))


    
    (progn
      (close-all-plots)
      (plot whole-xs_* whole-ys_n**
            ";use of additional styles;with points pt 0.1 ps 0.1 lc 'black'") ;; todo: point plot
      ;;(close-all-plots))
      )
    ;; (values whole-ys_n** whole-xs_*)
    nil
    ))


(defun chaos-tests ()
  (in-package :chaos1)
  ;; (plot-series) ;; todo: input parameter
  ;;(plot-cascade-orbit -2 4 0.005)
  (plot-cascade-orbit 3.33 (- 4 1.00e-5) 0.002)
  )





;;(defun cobweb-diaglam (val-r iter-max)
;;  )

#|
;; plot examples
(progn
  (close-all-plots)
  ;;(plot #(1 2 3 4) #(2 4 3 1) ";use of additional styles;with linespoints pt 7 ps 2 lc 'red'")
  (plot #(1 2 3 4) #(2 4 3 1) #(4 3 1 2)
        ";use of additional styles;with points pt 7 ps 2 lc 'red'")
  (plot #(1 2 3 4) #(4 2 1 3) "" #(1 2 3 4) #(1 2 3 4) "")
  ;;(close-all-plots))
  (plot #(1  3  2 4) #(4 2 1.5 3) #(1 2 3 4) #(1 2 3 4)
        ";use of additional styles;with points pt 7 ps 2 lc 'red'")
  ;;(plot #(1 2 3 4) #(2 4 3 1)
  ;;";use of additional styles;with points pt 1")
  )
|#

;; references
;; 1 about logiscic things: https://ja.wikipedia.org/wiki/ロジスティック写像
;; 2 plotting by gnuplot on lisp: https://github.com/volkers/vgplot


