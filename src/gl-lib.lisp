(in-package :fractal)


;; util

(defmacro nested-loop (syms dimensions &body body)
  "Iterates over a multidimensional range of indices.
   
   SYMS must be a list of symbols, with the first symbol
   corresponding to the outermost loop. 
   
   DIMENSIONS will be evaluated, and must be a list of 
   dimension sizes, of the same length as SYMS.

   Example:
    (nested-loop (i j) '(10 20) (format t '~a ~a~%' i j))

   Copied:
    https://lispcookbook.github.io/cl-cookbook/arrays.html

  "
  (unless syms (return-from nested-loop `(progn ,@body))) ; No symbols
  
  ;; Generate gensyms for dimension sizes
  (let* ((rank (length syms))
         (syms-rev (reverse syms)) ; Reverse, since starting with innermost
         (dims-rev (loop for i from 0 below rank collecting (gensym))) ; innermost dimension first
         (result `(progn ,@body))) ; Start with innermost expression
    ;; Wrap previous result inside a loop for each dimension
    (loop for sym in syms-rev for dim in dims-rev do
         (unless (symbolp sym) (error "~S is not a symbol. First argument to nested-loop must be a list of symbols" sym))
         (setf result
               `(loop for ,sym from 0 below ,dim do
                     ,result)))
    ;; Add checking of rank and dimension types, and get dimensions into gensym list
    (let ((dims (gensym)))
      `(let ((,dims ,dimensions))
         (unless (= (length ,dims) ,rank) (error "Incorrect number of dimensions: Expected ~a but got ~a" ,rank (length ,dims)))
         (dolist (dim ,dims)
           (unless (integerp dim) (error "Dimensions must be integers: ~S" dim)))
         (destructuring-bind ,(reverse dims-rev) ,dims ; Dimensions reversed so that innermost is last
           ,result)))))


;; mandelbrot set 

(defparameter *none-diverge* 0)
(defparameter *does-diverge* -1)


(defun mandelbrot-divergence (complex num-iter)
  (labels
      ((m-set-iter (z[n-1] complex current-iter)
         (let ((z[n] (+ (expt z[n-1] 2) complex)))
           ;;(format t "~,20f  ~,20f ~%" (realpart z[n]) (imagpart z[n]))
           (cond ((<= 2.00 (abs z[n])) 
                  (values *does-diverge* current-iter))
                 ((< current-iter num-iter)
                  (m-set-iter z[n] complex (1+ current-iter)))
                 (t (values *none-diverge* current-iter))))))
    (m-set-iter (complex 0 0) complex 0)))


(defun quasi-mandelbrot (camera-x camera-y radius width height)
  (let*
      ((divergence-matrix (make-array (list width height)
                                      :element-type 'function :initial-element #'(lambda ()))))
    (nested-loop (dx dy) (array-dimensions divergence-matrix)
      (setf
       (aref divergence-matrix dx dy)
       (let ((x-part (+ camera-x (* radius (/ (- dx (/ width 2)) width))))
             (y-part (+ camera-y (* radius (/ (- dy (/ height 2)) height)))))
         #'(lambda ()
             (mandelbrot-divergence
              (complex x-part y-part)
              50)))))
    divergence-matrix))


(defun print-mandelbrot ()
  (let* ((width 60)
         (height 40)
         (set-M (quasi-mandelbrot 0.00 0.00 (* 2 2.00) width height)))
    (dotimes (y height)
      (fresh-line)
      (dotimes (x width)
        (multiple-value-bind (div iter)
            (funcall (aref set-M x y))
          (format t "~A"
                  (cond
                    ((= *none-diverge* div) "@")
                    ((> 10 iter) "_")
                    ((> 20 iter) "*")
                    (t "%"))))))))


;; for user interface

(defun hue-circle (diverge iter-count)
  "hue of cl-opengl's color function for fractal set"
  (let ((k (/ 1.00 2)))
    (if (= diverge *none-diverge*)
        (gl:color 0 0 0)
        (gl:color
         (sin (+ (* k iter-count) (/ (* 0 3) pi)))
         (sin (+ (* k iter-count) (/ (* 1 3) pi)))
         (sin (+ (* k iter-count) (/ (* 2 3) pi)))))))


;; opengl

(defparameter *default-window-width* 600)
(defparameter *default-window-height* 400)



(defclass mandelbrot-set-window (glut:window)
  ()
  (:default-initargs
   :width *default-window-width* :height *default-window-height*
   :mode '(:single :rgb) :title "mandelbrot set"))

(defmethod glut:display-window :before ((w mandelbrot-set-window))
  (gl:clear-color 0 0 0 0)
  )

(defmethod glut:display ((w mandelbrot-set-window))
  (gl:clear :color-buffer)
  (gl:flush)

  (gl:point-size 1)
  (gl:begin :points)

  (let* ((width 600)
         (height 400)
         (set-M (quasi-mandelbrot 0.00 0.00 (* 2 2.00) width height)))
    (nested-loop (x y) (array-dimensions set-M)
      (multiple-value-bind (div iter)
          (funcall (aref set-M x y))
        (hue-circle div iter))
      (gl:vertex (/ x width)
                 (/ y height))))
                
  (gl:end)
  (gl:flush))


(defmethod glut:reshape ((w mandelbrot-set-window) width height)
  (gl:viewport 0 0 width height)
  (gl:matrix-mode :projection)
  (gl:load-identity)
  (if (<= width height)
      (glu:ortho-2d 0 1 0 (* 1 (/ height width)))
      (glu:ortho-2d 0 (* 1 (/ width height)) 0 1))
  )

(defmethod glut:keyboard ((w mandelbrot-set-window) key x y)
  (declare (ignore x y))
  (case key
    (#\Esc (glut:destroy-current-window))))

(defun mandelbrot-set ()
  (glut:display-window (make-instance 'mandelbrot-set-window)))

